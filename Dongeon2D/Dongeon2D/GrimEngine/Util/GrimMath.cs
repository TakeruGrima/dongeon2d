﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrimEngine.Util
{
    static class GrimMath
    {
        static public int Clamp(int pValue,int pMin,int pMax)
        {
            if (pValue < pMin)
                pValue = pMin;
            if (pValue > pMax)
                pValue = pMax;

            return pValue;
        }
    }
}
