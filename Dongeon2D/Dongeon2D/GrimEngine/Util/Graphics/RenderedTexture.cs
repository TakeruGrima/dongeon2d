﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GrimEngine.Util.Graphics.SpriteBatchExtension;

namespace GrimEngine.Util.Graphics
{
    public class RenderedTexture
    {
        #region Fields

        // Public ----------------------------------------------------

        public int Width = 0;
        public int Height = 0;
        public Vector2 OldTargetPosition = Vector2.Zero;

        // Private ---------------------------------------------------

        private readonly GraphicsDevice mGraphicsDevice;
        private readonly SpriteBatch mSpriteBatch;

        private RenderTarget2D mTarget;

        #endregion

        #region Properties

        public Texture2D Texture { get; private set; }

        #endregion

        #region Constructors & Initialize

        public RenderedTexture(GraphicsDevice pGraphicsDevice)
        {
            mGraphicsDevice = pGraphicsDevice;
            mSpriteBatch = new SpriteBatch(pGraphicsDevice);
        }

        public RenderedTexture(GraphicsDevice pGraphicsDevice, int pWidth, int pHeight)
        {
            mGraphicsDevice = pGraphicsDevice;
            mSpriteBatch = new SpriteBatch(pGraphicsDevice);

            Width = pWidth;
            Height = pHeight;
        }

        #endregion

        #region Draw Methods

        public void DrawPoint(int pX, int pY, int pScale, Color pColor, float pOpacity)
        {
            InitRenderTarget();

            mSpriteBatch.DrawPoint(pX, pY, pScale, pColor, pOpacity);

            EndRenderTarget();
        }

        public void DrawPoint(Vector2 pPosition,int pScale, Color pColor, float pAlpha)
        {
            InitRenderTarget();

            mSpriteBatch.DrawPoint(pPosition,pScale, pColor, pAlpha);

            EndRenderTarget();
        }

        public void DrawLine(Vector2 pBegin, int pLenght, int pScale, Color pColor, eOrientation pOrientation)
        {
            InitRenderTarget();

            mSpriteBatch.DrawLine(pBegin, pLenght,pScale, pColor,pOrientation);

            EndRenderTarget();
        }

        public void DrawRectangle(Rectangle pRectangle, int pScale, bool pFill, Color pColor, float pAlpha)
        {
            InitRenderTarget();

            mSpriteBatch.DrawRectangle(pRectangle, pScale, pFill, pColor, pAlpha);

            EndRenderTarget();
        }

        public void DrawRectangle(Vector2 pPosition, int pWidth, int pHeight, int pScale, bool pFill, Color pColor, float pAlpha)
        {
            InitRenderTarget();

            mSpriteBatch.DrawRectangle(pPosition, pWidth, pHeight, pScale, pFill, pColor, pAlpha);

            EndRenderTarget();
        }

        public void DrawRectangle(int pX, int pY, int pWidth, int pHeight, int pScale, bool pFill, float pAlpha, Color pColor)
        {
            InitRenderTarget();

            mSpriteBatch.DrawRectangle(pX, pY, pWidth, pHeight, pScale, pFill, pAlpha, pColor);

            EndRenderTarget();
        }

        #endregion

        #region Methods

        public void Dispose()
        {
            if (mSpriteBatch != null)
                mSpriteBatch.Dispose();
            if (Texture != null)
                Texture.Dispose();
            if (mTarget != null)
                mTarget.Dispose();
            if (mSpriteBatch != null)
                mSpriteBatch.DisposeTexturePoint();
        }

        private void InitRenderTarget()
        {
            mTarget = new RenderTarget2D(mGraphicsDevice, Width, Height);

            mSpriteBatch.GraphicsDevice.SetRenderTarget(mTarget);

            mSpriteBatch.GraphicsDevice.Clear(Color.Transparent);

            mSpriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default);

            if (Texture != null)
            {
                mSpriteBatch.Draw(Texture, OldTargetPosition, Color.White);
            }
        }

        private void EndRenderTarget()
        {
            mSpriteBatch.End();

            mSpriteBatch.GraphicsDevice.SetRenderTarget(null);

            Texture = mTarget;
        }

        #endregion
    }
}
