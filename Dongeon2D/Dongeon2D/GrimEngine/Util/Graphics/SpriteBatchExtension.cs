﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrimEngine.Util.Graphics
{
    public static class SpriteBatchExtension
    {
        #region Fields

        public enum eOrientation { HORIZONTAL,VERTICAL}

        private static Texture2D mTexturePoint;

        #endregion

        #region Methods

        private static void InitTexturePoint(this SpriteBatch spriteBatch)
        {
            mTexturePoint = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
            mTexturePoint.SetData(new[] { Color.White });
        }

        public static void DisposeTexturePoint(this SpriteBatch spriteBatch)
        {
            if (mTexturePoint != null)
                mTexturePoint.Dispose();

            mTexturePoint = null;
        }

        public static void Draw(this SpriteBatch spriteBatch,RenderedTexture pTexture, Vector2 pPosition, Color pColor)
        {
            if (pTexture.Texture != null)
                spriteBatch.Draw(pTexture.Texture, pPosition, pColor);
        }

        public static void DrawPoint(this SpriteBatch spriteBatch,int pX, int pY, int pScale, Color pColor, float pOpacity)
        {
            if (mTexturePoint == null)
                InitTexturePoint(spriteBatch);

            spriteBatch.Draw(mTexturePoint, new Rectangle(pX, pY, 1 * pScale, 1 * pScale), pColor * pOpacity);
        }

        public static void DrawPoint(this SpriteBatch spriteBatch, Vector2 pPosition, int pScale, Color pColor, float pOpacity)
        {
            DrawPoint(spriteBatch,(int)pPosition.X, (int)pPosition.Y, pScale, pColor, pOpacity);
        }

        public static void DrawLine(this SpriteBatch spriteBatch, Vector2 pBegin,int pLenght,int pScale,Color pColor, eOrientation pOrientation )
        {
            if (mTexturePoint == null)
                InitTexturePoint(spriteBatch);

            int width = 1;
            int height = 1;

            switch (pOrientation)
            {
                case eOrientation.HORIZONTAL:
                    width = pLenght * pScale;
                    height = pScale;
                    break;
                case eOrientation.VERTICAL:
                    width = pScale;
                    height = pLenght * pScale;
                    break;
            }

            spriteBatch.Draw(mTexturePoint, new Rectangle((int)pBegin.X,(int)pBegin.Y, width, height), pColor);
        }

        public static void DrawRectangle(this SpriteBatch spriteBatch, int pX, int pY, int pWidth, int pHeight,
            int pScale, bool pFill, float pAlpha, Color pColor)
        {
            if (mTexturePoint == null)
                InitTexturePoint(spriteBatch);

            if (!pFill)
            {
                int bw = 2; // Border width

                spriteBatch.Draw(mTexturePoint, new Rectangle(pX, pY, bw, pHeight), pColor * pAlpha); // Left
                spriteBatch.Draw(mTexturePoint, new Rectangle(pX + pWidth, pY, bw, pHeight), pColor * pAlpha); // Right
                spriteBatch.Draw(mTexturePoint, new Rectangle(pX, pY, pWidth, bw), pColor * pAlpha); // Top
                spriteBatch.Draw(mTexturePoint, new Rectangle(pX, pY + pHeight, pWidth + bw, bw), pColor * pAlpha); // Bottom
            }
            else
                spriteBatch.Draw(mTexturePoint, new Rectangle(pX, pY, pWidth * pScale, pHeight * pScale), pColor * pAlpha);
        }

        public static void DrawRectangle(this SpriteBatch spriteBatch,Vector2 pPosition, int pWidth, int pHeight,
            int pScale, bool pFill, Color pColor, float pAlpha)
        {
            DrawRectangle(spriteBatch,(int)pPosition.X, (int)pPosition.Y, pWidth, pHeight, pScale, pFill, pAlpha, pColor);
        }

        public static void DrawRectangle(this SpriteBatch spriteBatch, Rectangle pRectangle, int pScale, bool pFill, Color pColor, float pAlpha)
        {
            DrawRectangle(spriteBatch,pRectangle.X, pRectangle.Y, pRectangle.Width, pRectangle.Height, pScale, pFill, pAlpha, pColor);
        }

        #endregion
    }
}
