﻿using GrimEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    public class Camera
    {
        #region Fields

        // Public ----------------------------------------------------------------------

        public static Camera Instance { get; set; }

        public Matrix ViewMatrix;

        public float TranslationSpeed = 60.0f;

        // Protected  ------------------------------------------------------------------

        // Private ---------------------------------------------------------------------

        private Vector2 mPosition;
        private float mZoom = 1;

        private Rectangle mZoneGame;
        private Viewport mView;

        #endregion

        #region Properties

        public float Zoom
        {
            get { return mZoom; }
            set
            {
                mZoom = value;
                if (mZoom < 0.1f)
                    Zoom = 0.1f;
            }
        }

        #endregion

        #region Constructors

        public Camera(Viewport pView)
        {
            mView = pView;

            mPosition = Vector2.Zero;

            mZoneGame = new Rectangle(0, 0, pView.Width, pView.Height);
        }

        #endregion

        #region Methods

        public static void Init(Vector2 pPosition, Rectangle pZoneGame)
        {
            Instance.mPosition = pPosition;

            Instance.mZoneGame = pZoneGame;
        }

        public static void Update(GameTime pGameTime)
        {
            /*float increment = (Instance.TranslationSpeed * (1 / Instance.Zoom)) * (float)pGameTime.ElapsedGameTime.TotalSeconds;

            if (Input.IsScrollDown() && Instance.Zoom >= 1)
            {
                Instance.Zoom -= 0.1f;
            }
            else if (Input.IsScrollUp() && Instance.Zoom <= 3)
            {
                Instance.Zoom += 0.1f;
            }

            if (Input.GetKey(Keys.Up))
            {
                Instance.mPosition.Y -= increment;
            }
            if (Input.GetKey(Keys.Down))
            {
                Instance.mPosition.Y += increment;
            }
            if (Input.GetKey(Keys.Right))
            {
                Instance.mPosition.X += increment;
            }
            if (Input.GetKey(Keys.Left))
            {
                Instance.mPosition.X -= increment;
            }

            Vector2 position = new Vector2
            {
                X = Instance.mPosition.X - (Instance.mView.Width / 2) / Instance.Zoom,
                Y = Instance.mPosition.Y - (Instance.mView.Height / 2) / Instance.Zoom
            };

            if (position.X <= Instance.mZoneGame.X)
                position.X = Instance.mZoneGame.X;
            if (position.Y <= Instance.mZoneGame.Y)
                position.Y = Instance.mZoneGame.Y;
            if (position.X + Instance.mView.Width / Instance.Zoom >= Instance.mZoneGame.X + Instance.mZoneGame.Width)
                position.X = Instance.mZoneGame.X + Instance.mZoneGame.Width - Instance.mView.Width / Instance.Zoom;
            if (position.Y + Instance.mView.Height / Instance.Zoom >= Instance.mZoneGame.Y + Instance.mZoneGame.Height)
                position.Y = Instance.mZoneGame.Y + Instance.mZoneGame.Height - Instance.mView.Height / Instance.Zoom;

            Instance.mPosition.X = position.X + (Instance.mView.Width / 2) / Instance.Zoom;
            Instance.mPosition.Y = position.Y + (Instance.mView.Height / 2) / Instance.Zoom;

            Instance.ViewMatrix = Matrix.CreateTranslation(new Vector3(-position, 0)) * Matrix.CreateScale(Instance.Zoom);*/
        }

        public static void Follow(IActor target, Rectangle pZoneGame)
        {
            Matrix translation;
            Matrix offset;

            float translatX, offsetX;
            float translatY, offsetY;

            float difScreenX = (pZoneGame.Width / 2 - Instance.mView.Width / 2);
            float difScreenY = (pZoneGame.Height / 2 - Instance.mView.Height / 2);

            if (target.Position.X + target.BoundingBox.Width / 2 > pZoneGame.Width / 2 + difScreenX
                || pZoneGame.Width < Instance.mView.Width)
            {
                translatX = -pZoneGame.Width / 2;
                offsetX = Instance.mView.Width / 2 - difScreenX;
            }
            else
            {
                if (target.Position.X + target.BoundingBox.Width / 2 < Instance.mView.Width / 2)
                {
                    translatX = 0;
                    offsetX = 0;
                }
                else
                {
                    translatX = -target.Position.X - (target.BoundingBox.Width / 2);
                    offsetX = Instance.mView.Width / 2;
                }
            }


            if (target.Position.Y + target.BoundingBox.Height / 2 > pZoneGame.Height / 2
                + difScreenY
                || pZoneGame.Height < Instance.mView.Height)
            {
                translatY = -pZoneGame.Height / 2;
                offsetY = Instance.mView.Height / 2 - difScreenY;
            }
            else
            {
                if (target.Position.Y + target.BoundingBox.Height / 2 < Instance.mView.Height / 2)
                {
                    translatY = 0;
                    offsetY = 0;
                }
                else
                {
                    translatY = -target.Position.Y - (target.BoundingBox.Height / 2);
                    offsetY = Instance.mView.Height / 2;
                }
            }

            translation = Matrix.CreateTranslation(
            translatX,
            translatY,
            0);

            offset = Matrix.CreateTranslation(offsetX, offsetY, 0);

            Instance.ViewMatrix = translation * offset;

            //Transform = translation * offset;
        }

        #endregion
    }
}
