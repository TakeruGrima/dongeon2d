﻿using Gamecodeur;
using GrimEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrimEngine
{
    public enum MouseButtons
    {
        Left,
        Right,
        Middle
    }

    public class Input
    {
        #region Static

        // Public ----------------------------------------------------

        static public MainGame mMainGame;

        // Private ---------------------------------------------------

        static KeyboardState mOldKBState;
        static KeyboardState mNewKBState;
        static MouseState mOldMouseState;
        static MouseState mNewMouseState;
        static GamePadState mOldGPState;
        static GamePadState mNewGPState;

        #endregion

        #region Fields

        // Public ----------------------------------------------------

        // Private ---------------------------------------------------

        #endregion

        #region Properties

        static public bool MouseActive
        {
            get
            {
                return mMainGame.IsActive;
            }
        }

        static public int MouseMoveX
        {
            get
            {
                return mNewMouseState.Position.X - mOldMouseState.Position.X;
            }
        }

        static public int MouseMoveY
        {
            get
            {
                return mNewMouseState.Position.Y - mOldMouseState.Position.Y;
            }
        }

        #endregion

        #region Constructors

        #endregion

        #region Methods

        // Public ----------------------------------------------------

        static public bool IsScrollDown()
        {
            if (mOldMouseState.ScrollWheelValue
                > mNewMouseState.ScrollWheelValue)
            {
                return true;
            }

            return false;
        }

        static public bool IsScrollUp()
        {
            if (mOldMouseState.ScrollWheelValue
                < mNewMouseState.ScrollWheelValue)
            {
                return true;
            }

            return false;
        }

        static public int ScrollWheelValue
        {
            get
            {
                if (mNewMouseState.ScrollWheelValue !=
                    mOldMouseState.ScrollWheelValue)
                {
                    return mNewMouseState.ScrollWheelValue;
                }

                return 0;
            }
        }

        static public void GetState()
        {
            mNewKBState = Keyboard.GetState();
            mNewMouseState = Mouse.GetState();

            GamePadCapabilities capabilities = GamePad.GetCapabilities(PlayerIndex.One);

            if (capabilities.IsConnected)
            {
                mNewGPState = GamePad.GetState(PlayerIndex.One,
                    GamePadDeadZone.IndependentAxes);
            }
        }

        static public void ReinitializeState()
        {
            mOldKBState = mNewKBState;
            mOldMouseState = mNewMouseState;
            mOldGPState = mNewGPState;
        }

        static public bool GetKeyUp(Keys pKey)
        {
            if (mNewKBState.IsKeyUp(pKey) && !mOldKBState.IsKeyUp(pKey))
            {
                return true;
            }
            return false;
        }

        static public bool GetKeyDown(Keys pKey)
        {
            if (mNewKBState.IsKeyDown(pKey) && !mOldKBState.IsKeyDown(pKey))
            {
                return true;
            }
            return false;
        }

        static public bool GetKey(Keys pKey)
        {
            if (mNewKBState.IsKeyDown(pKey) && mOldKBState.IsKeyDown(pKey))
            {
                return true;
            }
            return false;
        }

        static public Point MousePosition
        {
            get
            {
                return mNewMouseState.Position;
            }
        }

        static public Point MousePositionToWorld()
        {
            Point p = new Point
            (
                (int)Camera.Instance.ViewMatrix.Translation.X,
                (int)Camera.Instance.ViewMatrix.Translation.Y
            );

            Vector2 vector = (MousePosition - p).ToVector2();

            return ((1 / Camera.Instance.Zoom) * vector).ToPoint();
        }

        static public bool ClicOnZoneGame(Rectangle pZoneGame,
            MouseButtons pButton)
        {
            if (GetMouseButton(MouseButtons.Left)
                && pZoneGame.Contains(MousePosition))
            {
                if (MouseActive)
                {
                    return true;
                }
            }

            return false;
        }

        /*static public bool TestClicOnActor(GameObject pActor, MouseButtons pButton)
        {
            if (pActor.BoundingBox.Contains(MousePositionToWorld())
                && GetMouseButtonDown(pButton))
            {
                return true;
            }

            return false;
        }*/

        static public bool GetMouseButtonUp(MouseButtons pButtons)
        {
            switch (pButtons)
            {
                case MouseButtons.Left:
                    if (mNewMouseState.LeftButton == ButtonState.Released &&
                        mOldMouseState.LeftButton != ButtonState.Released)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Right:
                    if (mNewMouseState.RightButton == ButtonState.Released &&
                        mOldMouseState.RightButton != ButtonState.Released)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Middle:
                    if (mNewMouseState.MiddleButton == ButtonState.Released &&
                        mOldMouseState.MiddleButton != ButtonState.Released)
                    {
                        return true;
                    }
                    break;
                default:
                    break;
            }
            return false;
        }

        static public bool GetMouseButtonDown(MouseButtons pButtons)
        {
            switch (pButtons)
            {
                case MouseButtons.Left:
                    if (mNewMouseState.LeftButton == ButtonState.Pressed &&
                        mOldMouseState.LeftButton != ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Right:
                    if (mNewMouseState.RightButton == ButtonState.Pressed &&
                        mOldMouseState.RightButton != ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Middle:
                    if (mNewMouseState.MiddleButton == ButtonState.Pressed &&
                        mOldMouseState.MiddleButton != ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                default:
                    break;
            }
            return false;
        }

        static public bool GetMouseButton(MouseButtons pButtons)
        {
            switch (pButtons)
            {
                case MouseButtons.Left:
                    if (mNewMouseState.LeftButton == ButtonState.Pressed &&
                        mOldMouseState.LeftButton == ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Right:
                    if (mNewMouseState.RightButton == ButtonState.Pressed &&
                        mOldMouseState.RightButton == ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                case MouseButtons.Middle:
                    if (mNewMouseState.MiddleButton == ButtonState.Pressed &&
                        mOldMouseState.MiddleButton == ButtonState.Pressed)
                    {
                        return true;
                    }
                    break;
                default:
                    break;
            }
            return false;
        }

        static public bool GetButtonUp(Buttons pButton)
        {
            if (mNewGPState.IsButtonUp(pButton) && !mOldGPState.IsButtonUp(pButton))
            {
                return true;
            }
            return false;
        }

        static public bool GetButtonDown(Buttons pButton)
        {
            if (mNewGPState.IsButtonDown(pButton) && !mOldGPState.IsButtonDown(pButton))
            {
                return true;
            }
            return false;
        }

        static public bool GetButton(Buttons pButton)
        {
            if (mNewGPState.IsButtonDown(pButton) && mOldGPState.IsButtonDown(pButton))
            {
                return true;
            }
            return false;
        }

        #endregion
    }
}
