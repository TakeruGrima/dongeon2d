﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Gamecodeur
{
    public class Sprite : IActor
    {
        // IActor
        public Vector2 Position { get; set; }
        public Rectangle BoundingBox { get; private set; }
        public float vx;
        public float vy;

        // Sprite
        public Texture2D Texture { get; }
        public bool ToRemove { get; set; }

        public Sprite(Texture2D pTexture)
        {
            Texture = pTexture;
        }

        public void Move(float pX,float pY)
        {
            Position = new Vector2(Position.X + pX, Position.Y + pY);
        }

        public virtual void TouchBy(IActor pBy)
        {
            
        }

        public void UpdateBoundingBox()
        {
            BoundingBox = new Rectangle(
                (int)Position.X,
                (int)Position.Y,
                Texture.Width,
                Texture.Height
                );
        }

        public virtual void Draw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.Draw(Texture, Position, Color.White);
        }

        public virtual void Update(GameTime pGameTime)
        {
            Move(vx, vy);

            UpdateBoundingBox();
        }
    }
}
