﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrimEngine.Util.Graphics;

namespace Gamecodeur
{
    abstract public class Scene
    {
        #region Fields

        protected MainGame mMainGame;
        protected List<IActor> mListActors;
        protected List<IActor> mOverlayActors;

        protected Rectangle mScreen;

        #endregion

        #region Properties

        // Protected -------------------------------------------------

        protected ContentManager Content
        {
            get
            {
                return mMainGame.Content;
            }
        }

        protected SpriteBatch SpriteBatch
        {
            get
            {
                return mMainGame.SpriteBatch;
            }
        }

        #endregion

        #region Constructor

        public Scene(MainGame pGame)
        {
            mMainGame = pGame;
            mListActors = new List<IActor>();
            mOverlayActors = new List<IActor>();

            mScreen = mMainGame.Window.ClientBounds;

            Camera.Instance = new Camera(mMainGame.GraphicsDevice.Viewport);
        }

        #endregion

        #region Methods

        public void Clean()
        {
            mListActors.RemoveAll(item => item.ToRemove == true);
            mOverlayActors.RemoveAll(item => item.ToRemove == true);
        }


        public virtual void Destroy()
        {
            Debug.WriteLine("DESTROY");
        }

        protected void AddActor(IActor pActor)
        {
            mListActors.Add(pActor);
        }

        protected void AddOverlayActor(IActor pOverlaidActor)
        {
            mOverlayActors.Add(pOverlaidActor);
        }

        #endregion

        #region GrimEngine Methods

        public virtual void Load()
        {

        }

        public virtual void UnLoad()
        {
            SpriteBatch.DisposeTexturePoint();
        }

        public virtual void Update(GameTime gameTime)
        {
            Camera.Update(gameTime);

            foreach (IActor actor in mListActors)
            {
                actor.Update(gameTime);
            }

            foreach (IActor actor in mOverlayActors)
            {
                actor.Update(gameTime);
            }
        }

        public virtual void Draw(GameTime gameTime)
        {
            SpriteBatch.End();

            SpriteBatch.Begin(transformMatrix: Camera.Instance.ViewMatrix);

            foreach (IActor actor in mListActors)
            {
                actor.Draw(mMainGame.SpriteBatch);
            }

            SpriteBatch.End();

            SpriteBatch.Begin();

            foreach (IActor actor in mOverlayActors)
            {
                actor.Draw(mMainGame.SpriteBatch);
            }
        }

        #endregion
    }
}
