﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gamecodeur
{
    class AssetManager
    {
        #region Properties

        public static string ImagePath { get; private set; } = @"images\";
        public static string FontsPath { get; private set; } = @"fonts\";
        public static string SonsPath { get; private set; } = @"sons\";
        public static SpriteFont MainFont { get; private set; }
        public static Song MusicGameplay { get; private set; }

        #endregion

        #region Methods

        public static void Load(ContentManager pContent)
        {
            MainFont = pContent.Load<SpriteFont>(FontsPath + "mainfont");
            MusicGameplay = pContent.Load<Song>(SonsPath + "techno");
        }

        #endregion
    }
}
