﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using GrimEngine;
using GrimEngine.Util;
using Dongeon2D.Map;
using Dongeon2D.DynamicDongeon;
using GrimEngine.Util.Graphics;

namespace Gamecodeur
{
    class TmpPlayer : Sprite
    {
        public int Col;
        public int Lig;

        public TmpPlayer(Texture2D pTexture) : base(pTexture)
        {

        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            base.Draw(pSpriteBatch);
        }

        public override void TouchBy(IActor pBy)
        {
            base.TouchBy(pBy);
        }

        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);
        }
    }

    class SceneGameplay : Scene
    {
        /* private Song music;
         private SoundEffect sndExplode;*/

        Map mMap;
        OverlayMap mOverlayMap;
        TmpPlayer mPlayer;

        float time = 0;

        #region Constructor

        public SceneGameplay(MainGame pGame) : base(pGame)
        {
            Debug.WriteLine("Scene Gameplay");
            //AddActor(mOverlayMap);
        }

        #endregion

        #region Methods

        public override void Destroy()
        {
            base.Destroy();
        }

        #endregion

        #region GrimEngine Methods

        public override void Load()
        {
            Debug.WriteLine("SceneGameplay.Load");

            /* music = AssetManager.MusicGameplay;
             MediaPlayer.Play(music);
             MediaPlayer.IsRepeating = true;

             sndExplode = Content.Load<SoundEffect>(AssetManager.SonsPath + "explode");*/

            mMap = new Map(Content.Load<Texture2D>(AssetManager.ImagePath + "goodTileset"), mScreen.Width, mScreen.Height);
            mOverlayMap = new OverlayMap(mMap, Vector2.Zero, mScreen.Width, mScreen.Height);

            mPlayer = new TmpPlayer(Content.Load<Texture2D>(AssetManager.ImagePath + "player"));

            Random rand = new Random();

            Vector2 position = mMap.GetRandomPositionInARoom(rand);

            mPlayer.Col = (int)position.X / mMap.TileSize;
            mPlayer.Lig = (int)position.Y / mMap.TileSize;

            mPlayer.Position = position;

            AddActor(mMap);
            AddActor(mPlayer);
            AddOverlayActor(mOverlayMap);

            base.Load();
        }

        public override void UnLoad()
        {
            Debug.WriteLine("SceneGameplay.Unload");
            MediaPlayer.Stop();

            base.UnLoad();
        }

        public override void Update(GameTime gameTime)
        {
            GamePadCapabilities capabilities = GamePad.GetCapabilities(PlayerIndex.One);

            foreach (IActor actor in mListActors)
            {
                //     actor.Update(gameTime);
            }

            time += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if ((Input.GetKeyDown(Keys.Left) || (time > 0.20f && Input.GetKey(Keys.Left))) && mMap.IfCellIsFloor(mPlayer.Col - 1, mPlayer.Lig))
            {
                mPlayer.Move(-64, 0);
                mPlayer.Col -= 1;

                time = 0;
            }
            else if ((Input.GetKeyDown(Keys.Right) || (time > 0.20f && Input.GetKey(Keys.Right))) && mMap.IfCellIsFloor(mPlayer.Col + 1, mPlayer.Lig))
            {
                mPlayer.Move(64, 0);
                mPlayer.Col += 1;

                time = 0;
            }
            if ((Input.GetKeyDown(Keys.Up) || (time > 0.20f && Input.GetKey(Keys.Up))) && mMap.IfCellIsFloor(mPlayer.Col, mPlayer.Lig - 1))
            {
                mPlayer.Move(0, -64);
                mPlayer.Lig -= 1;

                time = 0;
            }
            else if ((Input.GetKeyDown(Keys.Down) || (time > 0.20f && Input.GetKey(Keys.Down))) && mMap.IfCellIsFloor(mPlayer.Col, mPlayer.Lig + 1))
            {
                mPlayer.Move(0, 64);
                mPlayer.Lig += 1;

                time = 0;
            }

            Camera.Follow(mPlayer, mMap.BoundingBox);

            Clean();

            base.Update(gameTime);

            //mOverlayMap.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {

            //mOverlayMap.Draw(SpriteBatch);
            //mMap.Draw(SpriteBatch);
            base.Draw(gameTime);

            SpriteBatch.DrawPoint(mPlayer.Col * 10, mPlayer.Lig * 10, 10, Color.Yellow, 1);
        }

        #endregion
    }
}
