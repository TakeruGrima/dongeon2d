﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace Gamecodeur
{
    class SceneMenu : Scene
    {
        private KeyboardState oldKBState;
        private GamePadState oldGPState;
        private Song music;

        public SceneMenu(MainGame pGame) : base(pGame)
        {
            Debug.WriteLine("New Scene Menu");
        }

        public override void Load()
        {
            Debug.WriteLine("SceneMenu.Load");

            music = mMainGame.Content.Load<Song>("cool");
            MediaPlayer.Play(music);
            MediaPlayer.IsRepeating = true;

            Rectangle Screen = mMainGame.Window.ClientBounds;

            oldKBState = Keyboard.GetState();
            oldGPState = GamePad.GetState(PlayerIndex.One, GamePadDeadZone.IndependentAxes);

            base.Load();
        }

        public override void UnLoad()
        {
            Debug.WriteLine("SceneMenu.Unload");
            MediaPlayer.Stop();
            base.UnLoad();
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState newKbState = Keyboard.GetState();
            GamePadCapabilities capabilities = GamePad.GetCapabilities(PlayerIndex.One);
            GamePadState newGPState;
            bool ButA = false;

            if (capabilities.IsConnected)
            {
                newGPState = GamePad.GetState(PlayerIndex.One,
                    GamePadDeadZone.IndependentAxes);

                if(newGPState.IsButtonDown(Buttons.A) && !oldGPState.IsButtonDown(Buttons.A))
                {
                    ButA = true;
                }
            }

            MouseState newMState = Mouse.GetState();
            if(newMState.LeftButton == ButtonState.Pressed)
            {

            }

            if ((newKbState.IsKeyDown(Keys.Enter) && !oldKBState.IsKeyDown(Keys.Enter))
                || ButA)
            {
                mMainGame.GameState.ChangeScene(GameState.SceneType.Gameplay);
            }

            oldKBState = newKbState;

            if(capabilities.IsConnected)
            {
                oldGPState = GamePad.GetState(PlayerIndex.One,
                    GamePadDeadZone.IndependentAxes);
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            mMainGame.SpriteBatch.DrawString(AssetManager.MainFont,
                "This is the menu!", new Vector2(1, 1), Color.White);

            base.Draw(gameTime);
        }

        public override void Destroy()
        {
            base.Destroy();
        }
    }
}
