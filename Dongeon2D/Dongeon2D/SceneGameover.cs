﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Media;

namespace Gamecodeur
{
    class SceneGameover : Gamecodeur.Scene
    {
        private Song music;

        public SceneGameover(MainGame pGame) : base(pGame)
        {
            Debug.WriteLine("New Scene Gameover");
        }

        public override void Load()
        {
            Debug.WriteLine("SceneGameover.Load");

            music = mMainGame.Content.Load<Song>(AssetManager.SonsPath + "cool");
            MediaPlayer.Play(music);
            MediaPlayer.IsRepeating = true;

            base.Load();
        }

        public override void UnLoad()
        {
            Debug.WriteLine("SceneGameover.Unload");
            MediaPlayer.Stop();
            base.UnLoad();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            mMainGame.SpriteBatch.DrawString(AssetManager.MainFont,
                "GAMEOVER!", new Vector2(1, 1), Color.White);

            base.Draw(gameTime);
        }

        public override void Destroy()
        {
            base.Destroy();
        }
    }
}
