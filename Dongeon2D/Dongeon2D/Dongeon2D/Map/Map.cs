﻿using Dongeon2D.DynamicDongeon;
using Gamecodeur;
using GrimEngine;
using GrimEngine.Util.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dongeon2D.DynamicDongeon.DFS;

namespace Dongeon2D.Map
{
    class Map : IActor
    {
        #region Fields

        // Public ---------------------------------------------

        public Vector2 Position { get; set; }
        public Rectangle BoundingBox { get; private set; }
        public bool ToRemove { get; set; }

        public int NbCol { get; private set; }
        public int NbLig { get; private set; }

        // Private ---------------------------------------------

        private Tileset mTileset;

        private Rectangle mFloorTile;
        private Rectangle mWallTile;

        public DongeonDFS mDongeonDFS;

        #endregion

        #region Properties

        public eCellType[,] Grid
        {
            get
            {
                return mDongeonDFS.Grid;
            }
        }

        public int CellSize { get; private set; }
        public int TileSize { get; private set; } = 64;

        #endregion

        #region Constructors

        public Map(Texture2D pTexture,int pMapWidth,int pMapHeight)
        {
            CellSize = 10;
            NbCol = pMapWidth / CellSize;
            NbLig = pMapHeight / CellSize;

            mTileset = new Tileset(pTexture, 64);
            mDongeonDFS = new DongeonDFS(NbCol, NbLig, CellSize, 8);

            NbLig = mDongeonDFS.NbLig;
            NbCol = mDongeonDFS.NbCol;

            //mWallTile = new Rectangle(3 * TileSize, 2 * TileSize, TileSize, TileSize);
            mFloorTile = new Rectangle(3 * TileSize, 2 * TileSize, TileSize, TileSize);

            BoundingBox = new Rectangle(0, 0, NbCol * TileSize, NbLig * TileSize);
        }

        #endregion

        #region Methods

        public bool IfCellIsFloor(int pCol, int pLine)
        {
            return mDongeonDFS.IfCellIsFloor(pCol, pLine);
        }

        public bool IfCellIsWall(int pCol, int pLine)
        {
            return mDongeonDFS.IfCellIsWall(pCol, pLine);
        }

        public Vector2 GetRandomPositionInARoom(Random mRand)
        {
            int idRoom = mRand.Next(0, mDongeonDFS.mRooms.Count);

            Room room = mDongeonDFS.mRooms[idRoom];

            int col = mRand.Next(room.PosX, room.PosX + room.NbCol);
            int lig = mRand.Next(room.PosY, room.PosY + room.NbLig);

            return new Vector2(col * TileSize, lig * TileSize);
        }

        #endregion

        #region GrimEngine Methods

        public void TouchBy(IActor pBy)
        {

        }

        public void Update(GameTime gameTime)
        {
            if (Input.GetKeyDown(Keys.A))
            {
                mDongeonDFS.CreateDongeon(NbCol, NbLig, 50, 8);
            }

            mDongeonDFS.Update(gameTime);
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            Vector2 pos = new Vector2(0, 0);

            for (int l = 0; l < NbLig; l++)
            {
                for (int c = 0; c < NbCol; c++)
                {
                    pos.X = TileSize * c;
                    pos.Y = TileSize * l;

                    if (IfCellIsFloor(c, l))
                    {
                        mTileset.DrawTile(pSpriteBatch, Position + pos, 3, 2);
                    }
                    else
                    {
                        // This cell is a wall

                        bool left = IfCellIsFloor(c - 1, l);
                        bool right = IfCellIsFloor(c + 1, l);
                        bool top = IfCellIsFloor(c, l - 1);
                        bool bottom = IfCellIsFloor(c, l + 1);

                        if (IfCellIsFloor(c-1,l))
                            mTileset.DrawTile(pSpriteBatch, Position + pos, 0, 1);
                        if (IfCellIsFloor(c + 1, l))
                            mTileset.DrawTile(pSpriteBatch, Position + pos, 2, 1);
                        if (IfCellIsFloor(c, l - 1))
                            mTileset.DrawTile(pSpriteBatch, Position + pos, 1, 0);
                        if (IfCellIsFloor(c, l + 1))
                            mTileset.DrawTile(pSpriteBatch, Position + pos, 1, 2);
                    }
                }
            }

            mDongeonDFS.Draw(pSpriteBatch);
        }

        #endregion
    }
}
