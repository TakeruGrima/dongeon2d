﻿using Gamecodeur;
using GrimEngine;
using GrimEngine.Util.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dongeon2D.DynamicDongeon.DFS;
using static GrimEngine.Util.Graphics.SpriteBatchExtension;

namespace Dongeon2D.Map
{
    class OverlayMap: IActor
    {
        #region Fields

        // Public ---------------------------------------------

        public Vector2 Position { get; set; }
        public Rectangle BoundingBox { get; private set; }
        public bool ToRemove { get; set; }

        // Private --------------------------------------------

        private Map mMap;
        private int mCellSize;

        private bool mUseDebug = false;

        private List<MapEntity> mEntities;

        #endregion

        #region Properties

        #endregion

        #region Constructors

        public OverlayMap(Map pMap,Vector2 pPosition,int pWidth,int pHeight)
        {
            mMap = pMap;
            Position = pPosition;

            mCellSize = Math.Min(pWidth / mMap.NbCol, pHeight / mMap.NbLig);

            mEntities = new List<MapEntity>();
        }

        #endregion

        #region Methods

        // Public --------------------------------------------------------------------

        public void AddEntity(int pID,int pCol,int pLig,Color pColor)
        {

        }

        // Private -------------------------------------------------------------------

        private void DrawWalls(SpriteBatch pSpriteBatch,int col,int line,Color pColor)
        {
            Vector2 pos;
            pos.X = mCellSize * col;
            pos.Y = mCellSize * line;

            if (line > 0 && mMap.IfCellIsWall(col,line - 1))
            {
                Vector2 wallPos = pos;
                wallPos.Y -= 2;
                DrawWall(pSpriteBatch, wallPos, eOrientation.HORIZONTAL, pColor);
            }
            if (line < mMap.NbLig - 1 && mMap.IfCellIsWall(col, line + 1))
            {
                Vector2 wallPos = pos;
                wallPos.Y += mCellSize;
                DrawWall(pSpriteBatch, wallPos, eOrientation.HORIZONTAL, pColor);
            }
            if (col > 0 && mMap.IfCellIsWall(col - 1, line))
            {
                Vector2 wallPos = pos;
                wallPos.X -= 2;
                DrawWall(pSpriteBatch, wallPos, eOrientation.VERTICAL, pColor);
            }
            if (col < mMap.NbCol - 1 && mMap.IfCellIsWall(col + 1, line))
            {
                Vector2 wallPos = pos;
                wallPos.X += mCellSize;
                DrawWall(pSpriteBatch, wallPos, eOrientation.VERTICAL, pColor);
            }
        }

        private void DrawWall(SpriteBatch pSpriteBatch,Vector2 pPos,eOrientation pOrientation,Color pColor)
        {
            pSpriteBatch.DrawLine(pPos, mCellSize / 2, 2, pColor, pOrientation);
        }

        #endregion

        #region GrimEngine Methods

        public void TouchBy(IActor pBy)
        {

        }

        public void Update(GameTime pGameTime)
        {

        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            Vector2 pos = new Vector2(0, 0);

            Color color = Color.White;

           // for (int i = 0; i < mMap.mDongeonDFS.mBsp.Areas.Count; i++)
           // {
           //     pSpriteBatch.DrawRectangle(mMap.mDongeonDFS.mBsp.Areas[i], 1, false, Color.Red, 1);
           // }

           // for (int i = 0; i < mMap.mDongeonDFS.mRooms.Count; i++)
           // {
           //     mMap.mDongeonDFS.mRooms[i].Draw(pSpriteBatch);
           // }


           //for (int i = 0; i < mMap.mDongeonDFS.mTestPoints.Count; i++)
           // {
           //     Color tmp = Color.Blue;
           //     if (i == 0)
           //         tmp = Color.Violet;
           //     else if (i == mMap.mDongeonDFS.mTestPoints.Count - 1)
           //         tmp = Color.Green;

           //     pSpriteBatch.DrawPoint(mMap.mDongeonDFS.mTestPoints[i].ToVector2() * mCellSize, mCellSize, tmp, 1);
           // }

            if (mUseDebug)
            {
                Color curr = Color.Orange;
                for (int l = 0; l < mMap.NbLig; l++)
                {
                    for (int c = 0; c < mMap.NbCol; c++)
                    {
                        pos.X = mCellSize * c;
                        pos.Y = mCellSize * l;

                        pSpriteBatch.DrawPoint(pos, mCellSize, curr, 1);
                        if (curr == Color.Orange)
                            curr = Color.Red;
                        else
                            curr = Color.Orange;
                    }
                }
            }

            for (int l = 0; l < mMap.NbLig; l++)
            {
                for (int c = 0; c < mMap.NbCol; c++)
                {
                    pos.X = mCellSize * c;
                    pos.Y = mCellSize * l;

                    if(mUseDebug)
                    {
                        /*if (mMap.IfCellIsFloor(c,l))
                        {
                            pSpriteBatch.DrawPoint(pos, mCellSize, Color.Blue, 1);
                        }
                        if (mMap.Grid[l,c] == eCellType.WALL_ROOM)
                        {
                            pSpriteBatch.DrawPoint(pos, mCellSize, Color.DarkRed, 1);
                        }
                        Vector2 wallPos = pos;
                        DrawWall(pSpriteBatch, wallPos, eOrientation.HORIZONTAL, color);
                        wallPos.Y += mCellSize;
                        DrawWall(pSpriteBatch, wallPos, eOrientation.HORIZONTAL, color);
                        DrawWall(pSpriteBatch, wallPos, eOrientation.VERTICAL, color);
                        wallPos.X += mCellSize - 2;
                        DrawWall(pSpriteBatch, wallPos, eOrientation.VERTICAL, color);*/

                        if (mMap.IfCellIsFloor(c, l))
                        {
                            DrawWalls(pSpriteBatch, c, l, color);
                        }
                    }
                    else
                    {
                        if (mMap.IfCellIsFloor(c, l))
                        {
                            DrawWalls(pSpriteBatch, c, l, color);
                        }
                    }
                }
            }
        }

        #endregion
    }
}
