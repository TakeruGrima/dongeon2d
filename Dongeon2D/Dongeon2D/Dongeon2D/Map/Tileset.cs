﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dongeon2D.Map
{
    class Tileset
    {
        #region Fields

        private Texture2D mTexture;
        private int mNbCol;
        private int mNbLig;
        private int mTileSize;

        #endregion

        #region Properties

        #endregion

        #region Constructors

        public Tileset(Texture2D pTexture,int pTileSize)
        {
            mTexture = pTexture;
            mNbCol = pTexture.Width / pTileSize;
            mNbLig = pTexture.Height / pTileSize;
            mTileSize = pTileSize;
        }

        #endregion

        #region Methods

        #endregion

        #region GrimEngine Methods

        public void DrawTile(SpriteBatch pSpriteBatch,Vector2 pPosition, int pCol,int pLig)
        {
            if (pCol < 0 || pCol >= mNbCol || pLig < 0 || pLig >= mNbLig) return;

            Rectangle source = new Rectangle(pCol * mTileSize, pLig * mTileSize, mTileSize, mTileSize);

            pSpriteBatch.Draw(mTexture, pPosition, source, Color.White);
        }

        #endregion
    }
}
