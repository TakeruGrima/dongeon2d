﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dongeon2D.DynamicDongeon
{
    public class Door
    {
        #region Fields

        public enum eDoorSide { TOP, BOTTOM, LEFT, RIGHT }

        #endregion

        #region Properties

        public int RoomID { get; private set; }

        public eDoorSide Side { get; private set; }
        public int Col { get; private set; }
        public int Line { get; private set; }

        #endregion

        #region Constructor

        public Door(int pRoomID,eDoorSide pSide,int pCol,int pLine)
        {
            RoomID = pRoomID;
            Side = pSide;
            Col = pCol;
            Line = pLine;
        }

        #endregion

        #region Methods

        public Point GetPosition()
        {
            return new Point(Col, Line);
        }

        #endregion
    }
}
