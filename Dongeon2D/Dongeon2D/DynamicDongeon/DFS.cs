﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GrimEngine.Util.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Dongeon2D.DynamicDongeon
{
    public class DFS
    {
        #region Fields

        // Public --------------------------------------------

        public enum eCellType { WALL,WALL_ROOM, FLOOR,FLOOR_ROOM,VOID }

        // Protected -----------------------------------------

        public enum eDirection { UP, DOWN, LEFT, RIGHT,DEFAULT }
        protected eDirection mLastDirection;

        protected Random mRand;

        protected int mNbExplored = 0;
        protected int mMaxCells;

        // Private -------------------------------------------

        #endregion

        #region Properties

        // Public --------------------------------------------

        public eCellType[,] Grid { get; protected set; }

        // Protected -----------------------------------------

        public int NbCol { get; private set; }
        public int NbLig { get; private set; }

        #endregion

        #region Constructor

        public DFS(int pNbCol,int pNbLig)
        {
            Init(pNbCol, pNbLig);
        }

        #endregion

        #region Methods

        // Public ------------------------------------------------------

        public void Maze(int pBeginCol,int pBeginLine)
        {
            // Start the maze!

            Debug.WriteLine("Before Maze  mNbExplored :" + mNbExplored);

            //mLastDirection = (eDirection)mRand.Next((int)eDirection.LEFT, (int)eDirection.DOWN + 1);
            mLastDirection = eDirection.DEFAULT;

            Explore(pBeginCol, pBeginLine);
        }

        // Protected

        protected void Init(int pNbCol, int pNbLig)
        {
            mRand = new Random(1);
            mNbExplored = 0;

            // Init grid
            InitGrid(pNbCol, pNbLig);
        }

        protected virtual eDirection ChooseDirection(List<eDirection> pDirs,int pCol,int pLig)
        {
            int id = -1;

            if (pDirs.Contains(mLastDirection))
            {
                int choice = mRand.Next(0, 100);

                if (choice < 80)
                    id = pDirs.IndexOf(mLastDirection);
            }

            if(id == -1)
                id = mRand.Next(0, pDirs.Count);

            return pDirs[id];
        }

        public void DigWall(int pCol, int pLine)
        {
            if (pCol < 0 || pCol >= NbCol || pLine < 0 || pLine >= NbLig) return;
            
            Grid[pLine, pCol] = eCellType.FLOOR;
        }

        // Private

        private void InitGrid(int pNbCol,int pNbLig)
        {
            NbCol = pNbCol;
            if (NbCol % 2 == 0) NbCol--;

            NbLig = pNbLig;
            if (NbLig % 2 == 0) NbLig--;

            mMaxCells = (int)Math.Floor((NbLig * NbCol) / 2.0f);

            Grid = new eCellType[NbLig, NbCol];

            // Fill grid

            for (int l = 0; l < NbLig; l++)
            {
                for (int c = 0; c < NbCol; c++)
                {
                    Grid[l, c] = eCellType.WALL;
                }
            }
        }

        protected List<eDirection> GetDirections(int pCol, int pLine)
        {
            if (pCol < 0 || pCol >= NbCol || pLine < 0 || pLine >= NbLig) return null;

            List<eDirection> freeDirections = new List<eDirection>();

            if (pLine > 1 && Grid[pLine - 2, pCol] == eCellType.WALL)
            {
                freeDirections.Add(eDirection.UP);
            }
            if (pLine < (NbLig - 2) && Grid[pLine + 2, pCol] == eCellType.WALL)
            {
                freeDirections.Add(eDirection.DOWN);
            }
            if (pCol > 1 && Grid[pLine, pCol - 2] == eCellType.WALL)
            {
                freeDirections.Add(eDirection.LEFT);
            }
            if (pCol < (NbCol - 2) && Grid[pLine, pCol + 2] == eCellType.WALL)
            {
                freeDirections.Add(eDirection.RIGHT);
            }

            return freeDirections;
        }

        private void Explore(int pCol, int pLine)
        {
            DigWall(pCol, pLine);
            mNbExplored++;

            while (mNbExplored < mMaxCells)
            {
                List <eDirection> dirs = GetDirections(pCol, pLine);

                if (dirs.Count > 0)
                {
                    mLastDirection = ChooseDirection(dirs,pCol,pLine);

                    switch (mLastDirection)
                    {
                        case eDirection.UP:
                            DigWall(pCol, pLine - 1);
                            Explore(pCol, pLine - 2);
                            break;
                        case eDirection.DOWN:
                            DigWall(pCol, pLine + 1);
                            Explore(pCol, pLine + 2);
                            break;
                        case eDirection.LEFT:
                            DigWall(pCol - 1, pLine);
                            Explore(pCol - 2, pLine);
                            break;
                        case eDirection.RIGHT:
                            DigWall(pCol + 1, pLine);
                            Explore(pCol + 2, pLine);
                            break;
                        default:
                            break;
                    }
                }
                else
                    break;
            }
        }

        #endregion
    }
}
