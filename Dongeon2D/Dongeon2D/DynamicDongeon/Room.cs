﻿using GrimEngine.Util.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dongeon2D.DynamicDongeon.DFS;

namespace Dongeon2D.DynamicDongeon
{
    public class Room
    {
        #region Fields

        // Private ------------------------------------------

        private DongeonDFS mDongeon;
        private readonly int mGap;
        private readonly int mMinSize;
        private readonly int mCellSize;

        private Random mRand;
        private Rectangle mGridArea;
        private Rectangle mRoomRectangle;

        #endregion

        #region Properties

        // Public ------------------------------------------

        public int ID { get; private set; }
        //public eCellType[,] Grid { get; private set; }

        public int PosX { get; private set; }
        public int PosY { get; private set; }
        public int NbCol { get; private set; }
        public int NbLig { get; private set; }

        public Point Center { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pID">Room identifier</param>
        /// <param name="pAreaRectangle">Area that contains the room</param>
        /// <param name="pCellSize">Size of a cell in the map</param>
        /// <param name="pMapSize">Size of the map</param>
        /// <param name="pGap">number of cells next to the edge of the map</param>
        public Room(int pID,Rectangle pAreaRectangle,int pMinSize, int pCellSize,DongeonDFS pDongeon, int pGap, Random pRand)
        {
            ID = pID;
            mCellSize = pCellSize;
            mGap = pGap;
            mDongeon = pDongeon;
            mMinSize = pMinSize;

            mRand = pRand;

            mGridArea = DefineAreaInGrid(pAreaRectangle);

            CreateRoom();

            mRoomRectangle = new Rectangle(PosX, PosY, NbCol,NbLig);

            DigRoom();

            int centerX = PosX + NbCol / 2;
            int centerY = PosY + NbLig / 2;

            if (centerX % 2 == 0)
                centerX++;
            if (centerY % 2 == 0)
                centerY++;

            Center = new Point(centerX, centerY);

        }

        #endregion

        #region Methods

        // Public -------------------------------------------------------------

        public bool Contains(Point p)
        {
            return mRoomRectangle.Contains(p);
        }

        // Private -------------------------------------------------------------

        private void PlaceRoomWall(int pCol, int pLig)
        {
            if (pCol < 0 || pCol >= mDongeon.NbCol || pLig < 0 || pLig >= mDongeon.NbLig) return;
            mDongeon.Grid[pLig, pCol] = eCellType.WALL_ROOM;
        }

        private void DigWall(int pCol, int pLig)
        {
            if (pCol < 0 || pCol >= mDongeon.NbCol || pLig < 0 || pLig >= mDongeon.NbLig) return;
            mDongeon.Grid[pLig, pCol] = eCellType.FLOOR_ROOM;
        }

        private Rectangle DefineAreaInGrid(Rectangle pArea)
        {
            // add mGap or remove avoid the room being to close to edge of screen or another room
            int col = (int)Math.Ceiling((double)pArea.X / mCellSize) + mGap;
            int lig = (int)Math.Ceiling((double)pArea.Y / mCellSize) + mGap;

            int nbCol = (int)Math.Floor((double)pArea.Width / mCellSize) - 2 *mGap;
            int nbLig = (int)Math.Floor((double)pArea.Height / mCellSize) - 2 *mGap;

            if (col % 2 != 0)
            {
                col++;
                nbCol--;
            }
            if (lig % 2 != 0)
            {
                lig++;
                nbLig--;
            }

            if ((nbCol + col) % 2 != 0)
            {
                nbCol--;
            }

            if ((nbLig + lig) % 2 != 0)
            {
                nbLig--;
            }

            return new Rectangle(col, lig, nbCol, nbLig);
        }

        private void CreateRoom()
        {
            // Initialize minimal upper-left corner position and maximal bottom-right corner position
            Point minLeft = mGridArea.Location;
            Point maxRight = mGridArea.Location + mGridArea.Size;

            // initialize maxSize
            int maxWidth = mGridArea.Width;
            int maxHeight = mGridArea.Height;

            // Calculate size

            if (mMinSize > maxWidth)
                NbCol= maxWidth + 1;
            else
                NbCol = mRand.Next(mMinSize, maxWidth);

            if (mMinSize > maxHeight)
                NbLig = maxHeight;
            else
                NbLig = mRand.Next(mMinSize, maxHeight);

            // Calculate position

            PosX = mRand.Next(minLeft.X, maxRight.X - NbCol);
            PosY = mRand.Next(minLeft.Y, maxRight.Y - NbLig);

            if (PosX % 2 == 0)
            {
                PosX++;
            }
            if (PosY % 2 == 0)
            {
                PosY++;
            }

            if ((NbCol + PosX) % 2 != 0)
            {
                NbCol--;
            }

            if ((NbLig + PosY) % 2 != 0)
            {
                NbLig--;
            }
        }

        private void DigRoom()
        {
            Point pos;

            for (int l = 0; l < NbLig; l++)
            {
                for (int c = 0; c < NbCol; c++)
                {
                    pos = new Point(c + PosX, l + PosY);

                    DigWall(pos.X, pos.Y);
                    if (l == 0)
                        PlaceRoomWall(pos.X, pos.Y - 1);
                    if (c == 0)
                        PlaceRoomWall(pos.X - 1, pos.Y);
                    if (c == NbCol - 1)
                        PlaceRoomWall(pos.X + 1, pos.Y);
                    if (l == NbLig - 1)
                        PlaceRoomWall(pos.X, pos.Y + 1);
                }
            }

            pos = new Point(PosX,PosY);

            PlaceRoomWall(pos.X - 1, pos.Y - 1);
            PlaceRoomWall(pos.X - 1, pos.Y + NbLig);
            PlaceRoomWall(pos.X + NbCol, pos.Y - 1);
            PlaceRoomWall(pos.X + NbCol, pos.Y + NbLig);
        }

        #endregion

        public void Draw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.DrawRectangle(new Rectangle(mGridArea.X * mCellSize, mGridArea.Y * mCellSize, mGridArea.Width * mCellSize, mGridArea.Height * mCellSize), 1, false, Color.Blue, 1);
        }
    }
}
