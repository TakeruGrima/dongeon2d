﻿using AStarPathFinding;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dongeon2D.DynamicDongeon.DFS;

namespace Dongeon2D.DynamicDongeon
{
    using DebugPoint = System.Drawing.Point;

    class Coridor
    {
        #region Fields

        // Private -------------------------------------------------------

        private Door mBegin;
        private Door mEnd;
        private DongeonDFS mDongeon;

        private bool[,] mMap;
        private SearchParameters mSParameters;

        int cptTest = 0;

        #endregion

        #region Properties

        #endregion

        #region Constructor

        public Coridor(Door pBeginDoor,Door pEndDoor,DongeonDFS pDongeon)
        {
            mBegin = pBeginDoor;
            mEnd = pEndDoor;
            mDongeon = pDongeon;

            InitializeMap();

            mSParameters = new SearchParameters(new DebugPoint(mBegin.Col, mBegin.Line), new DebugPoint(mEnd.Col, mEnd.Line), mMap);

            PathFinder pathFinder = new PathFinder(mSParameters, 4);
            List<DebugPoint> path = pathFinder.FindPath();

            ShowRoute("The algorithm should find a direct path without obstacles:", path);

            if (path.Count == 0) return;

            DebugPoint currentPoint = path[0];

            mDongeon.DigWall(path[0].X,path[0].Y);
            path.RemoveAt(0);

            if(currentPoint.X % 2 == 0 || currentPoint.Y % 2 == 0)
            {
                currentPoint = path[0];
                mDongeon.DigWall(path[0].X, path[0].Y);
                path.RemoveAt(0);
            }

            DebugPoint endPoint = new DebugPoint(pEndDoor.Col, pEndDoor.Line);

            int cpt = 0;

            while (currentPoint != endPoint)
            {
                if (cpt == path.Count)
                {
                    break;
                }
                eDirection dir = GetDirection(currentPoint.X, currentPoint.Y, path[cpt]);

                bool digTwice = true;

                if (cpt == path.Count - 1)
                    digTwice = false;

                currentPoint = Dig(currentPoint, dir, true);

                cpt++;
            }
        }

        #endregion

        #region Methods

        private DebugPoint Dig(DebugPoint p,eDirection pDirection,bool pDigTwice)
        {
            switch (pDirection)
            {
                case eDirection.UP:
                    p.Y -= 1;
                    if (p.Y == mEnd.Line && (p.X - 1 == mEnd.Col || p.X + 1 == mEnd.Col))
                        pDigTwice = false;
                    break;
                case eDirection.DOWN:
                    p.Y += 1;
                    if (p.Y == mEnd.Line && (p.X - 1 == mEnd.Col || p.X + 1 == mEnd.Col))
                        pDigTwice = false;
                    break;
                case eDirection.LEFT:
                    p.X -= 1;
                    if (p.X == mEnd.Col && (p.Y - 1 == mEnd.Line || p.Y + 1 == mEnd.Line))
                        pDigTwice = false;
                    break;
                case eDirection.RIGHT:
                    p.X += 1;
                    if (p.X == mEnd.Col && (p.Y - 1 == mEnd.Line || p.Y + 1 == mEnd.Line))
                        pDigTwice = false;
                    break;
                default:
                    break;
            }

            mDongeon.DigWall(p.X, p.Y);
            if (pDigTwice)
            {
                p = Dig(p, pDirection, false);
            }

            return p;
        }

        private eDirection GetDirection(int pCol,int pLine,DebugPoint pNextPoint)
        {
            if(pCol + 2 < mDongeon.NbCol && pNextPoint.X > pCol && mDongeon.Grid[pLine,pCol + 2] != eCellType.WALL_ROOM)
            {
                return eDirection.RIGHT;
            }
            else if (pCol - 2 > 0 && pNextPoint.X < pCol && mDongeon.Grid[pLine, pCol - 2] != eCellType.WALL_ROOM)
            {
                return eDirection.LEFT;
            }
            else if (pLine + 2 < mDongeon.NbLig && pNextPoint.Y > pLine && mDongeon.Grid[pLine + 2, pCol] != eCellType.WALL_ROOM)
            {
                return eDirection.DOWN;
            }
            else if (pLine - 2 > 0 && pNextPoint.Y < pLine && mDongeon.Grid[pLine - 2, pCol] != eCellType.WALL_ROOM)
            {
                return eDirection.UP;
            }

            return eDirection.DEFAULT;
        }

        private void InitializeMap()
        {
            mMap = new bool[mDongeon.NbCol, mDongeon.NbLig];

            for (int l = 0; l < mDongeon.NbLig; l++)
            {
                for (int c = 0; c < mDongeon.NbCol; c++)
                {
                    if (mDongeon.Grid[l, c] == DFS.eCellType.WALL || mDongeon.Grid[l, c] == DFS.eCellType.FLOOR)
                        mMap[c, l] = true;
                    else
                        mMap[c, l] = false;
                }
            }
        }

        /// <summary>
        /// Displays the map and path as a simple grid to the console
        /// </summary>
        /// <param name="title">A descriptive title</param>
        /// <param name="path">The points that comprise the path</param>
        private void ShowRoute(string title, IEnumerable<System.Drawing.Point> path)
        {
            Console.WriteLine("{0}\r\n", title);
            for (int y = 0; y < mMap.GetLength(1); y++) // Invert the Y-axis so that coordinate 0,0 is shown in the bottom-left
            {
                for (int x = 0; x < mMap.GetLength(0); x++)
                {
                    if (mSParameters.EqualStart(x, y))
                        // Show the start position
                        Console.Write('S');
                    else if (mSParameters.EqualEnd(x, y))
                        // Show the end position
                        Console.Write('F');
                    else if (mMap[x, y] == false)
                        // Show any barriers
                        Console.Write('░');
                    else if (path.Where(p => p.X == x && p.Y == y).Any())
                        // Show the path in between
                        Console.Write('*');
                    else
                        // Show nodes that aren't part of the path
                        Console.Write('·');
                }

                Console.WriteLine();
            }
        }

        #endregion

        #region GrimEngine Methods

        #endregion
    }
}
