﻿using GrimEngine.Util.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GrimEngine.Util.Graphics.SpriteBatchExtension;

namespace Dongeon2D.DynamicDongeon
{
    public class BSP
    {
        #region Fields

        public bool UseDebug;

        private readonly int mMapWidth;
        private readonly int mMapHeight;

        private readonly int mMinRoomSize;

        private Random mRand;

        private eOrientation mCurrentCut = eOrientation.HORIZONTAL;

        #endregion

        #region Properties

        public List<Rectangle> Areas { get; private set; }

        #endregion

        #region Constructors

        public BSP(int pWidth,int pHeight,int pMaxRooms,int pMinRoomSize,bool pUseDebug)
        {
            mMapWidth = pWidth;
            mMapHeight = pHeight;
            UseDebug = pUseDebug;
            mMinRoomSize = pMinRoomSize;

            mRand = new Random();

            CreateSlices(pMaxRooms);
        }

        #endregion

        #region Methods

        private void CreateSlices(int pMaxSlices)
        {
            if (Areas != null)
                Areas.Clear();
            Areas = new List<Rectangle>();

            int nbSlices = mRand.Next(5, pMaxSlices + 1);

            List<int> roomsTested = new List<int>();

            int id = -1;

            while (Areas.Count < nbSlices)
            {
                Rectangle rect;
                if (id == -1)
                    rect = new Rectangle(0, 0, mMapWidth, mMapHeight);
                else
                {
                    id = 0;
                    if (Areas.Count > 1)
                        id = mRand.Next(0, Areas.Count);

                    rect = Areas[id];

                    if (Areas.Count > 0 && !roomsTested.Contains(id))
                        roomsTested.Add(id);
                }

                if (CutScreen(rect))
                {
                    id = 0;
                    if (Areas.Count > 0)
                    {
                        Areas.Remove(rect);

                        roomsTested.Remove(id);
                    }
                }

                if (roomsTested.Count == Areas.Count && id != -1)
                {
                    break;
                }
            }
        }

        private int GetSliceSize(float pOriginalSize)
        {
            return (int)Math.Floor(pOriginalSize * (mRand.Next(30, 70 + 1) / 100.0));
        }

        private bool CutScreen(Rectangle pPreviousRectangle)
        {
            int width1 = 0;
            int height1 = 0;

            int width2 = 0;
            int height2 = 0;

            Point pos1 = Point.Zero;
            Point pos2 = pos1;

            switch (mCurrentCut)
            {
                case eOrientation.HORIZONTAL:
                    width1 = GetSliceSize(pPreviousRectangle.Width);
                    width2 = pPreviousRectangle.Width - width1;

                    if (width1 < mMinRoomSize || width2 < mMinRoomSize)
                    {
                        width1 = pPreviousRectangle.Width / 2;
                        width2 = width1;

                        if(width1 < mMinRoomSize)
                        {
                            mCurrentCut = eOrientation.VERTICAL;
                            return false;
                        }
                    }

                    height1 = pPreviousRectangle.Height;

                    pos1.X = pPreviousRectangle.X;
                    pos1.Y = pPreviousRectangle.Y;

                    pos2.X = pos1.X + width1;
                    pos2.Y = pos1.Y;

                    Areas.Add(new Rectangle(pos1, new Point(width1, height1)));
                    Areas.Add(new Rectangle(pos2, new Point(width2, height1)));

                    mCurrentCut = eOrientation.VERTICAL;
                    return true;
                case eOrientation.VERTICAL:
                    height1 = GetSliceSize(pPreviousRectangle.Height);
                    height2 = pPreviousRectangle.Height - height1;

                    if (height1 < mMinRoomSize || height2 < mMinRoomSize)
                    {
                        height1 = pPreviousRectangle.Height / 2;
                        height2 = height1;

                        if (height1 < mMinRoomSize)
                        {
                            mCurrentCut = eOrientation.HORIZONTAL;
                            return false;
                        }
                    }

                    width1 = pPreviousRectangle.Width;

                    pos1.X = pPreviousRectangle.X;
                    pos1.Y = pPreviousRectangle.Y;

                    pos2.X = pos1.X;
                    pos2.Y = pos1.Y + height1;

                    Areas.Add(new Rectangle(pos1, new Point(width1, height1)));
                    Areas.Add(new Rectangle(pos2, new Point(width1, pPreviousRectangle.Height - height1)));

                    mCurrentCut = eOrientation.HORIZONTAL;
                    return true;
                default:
                    return false;
            }
        }

        #endregion
    }
}
