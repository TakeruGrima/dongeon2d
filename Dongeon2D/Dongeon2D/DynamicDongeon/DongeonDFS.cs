﻿using GrimEngine;
using GrimEngine.Util;
using GrimEngine.Util.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GrimEngine.Util.Graphics.SpriteBatchExtension;

namespace Dongeon2D.DynamicDongeon
{
    public class DongeonDFS : DFS
    {
        #region Fields

        private int mMinWidth = 5;
        private int mMinHeight = 5;
        private int mMaxWidth = 8;
        private int mMaxHeight = 8;

        private int mCellSize;

        private int mMaxRooms = 8;
        public List<Room> mRooms;

        public BSP mBsp;

        public List<Point> mTestPoints;

        #endregion

        #region Properties

        #endregion

        #region Constructors

        public DongeonDFS(int pNbCol, int pNbLig, int pCellSize, int pMaxRooms) : base(pNbCol, pNbLig)
        {
            CreateDongeon(pNbCol, pNbLig, pCellSize, pMaxRooms);
        }

        #endregion

        #region Methods

        // Public ------------------------------------------------------

        public bool IfCellIsFloor(int pCol, int pLine)
        {
            if (pCol < 0 || pCol >= NbCol || pLine < 0 || pLine >= NbLig)
                return false;

            eCellType cell = Grid[pLine, pCol];
            if (cell == eCellType.FLOOR_ROOM || cell == eCellType.FLOOR)
                return true;
            return false;
        }

        public bool IfCellIsWall(int pCol, int pLine)
        {
            if (pCol < 0 || pCol >= NbCol || pLine < 0 || pLine >= NbLig)
                return false;

            eCellType cell = Grid[pLine, pCol];
            if (cell == eCellType.WALL || cell == eCellType.WALL_ROOM)
                return true;
            return false;
        }

        public void CreateDongeon(int pNbCol, int pNbLig, int pCellSize, int pMaxRooms)
        {
            Init(pNbCol, pNbLig);

            mRand = new Random(1);

            mCellSize = pCellSize;

            mRooms = new List<Room>();

            int minSizeRoomsInPixel = (mMinWidth + 6) * mCellSize;

            mBsp = new BSP(pNbCol * pCellSize, pNbLig * pCellSize, mMaxRooms, minSizeRoomsInPixel, false);

            for (int i = 0; i < mBsp.Areas.Count; i++)
            {
                Rectangle rect = mBsp.Areas[i];

                mRooms.Add(new Room(i, rect,5, mCellSize, this,1,mRand));
            }

            CreateRoomsTunnel();

            /*int nbRandomPath = mRand.Next(1, 10);

            for (int i = 0; i < mBsp.Areas.Count; i++)
            {
                int randomId = mRand.Next(1, mRooms.Count);

                Room room = mRooms[randomId - 1];

                int previousRoomCenterX = room.Center.X;
                int previousRoomCenterY = room.Center.Y;

                Point pos = new Point(previousRoomCenterX, previousRoomCenterY);
                
                pos.X = mBsp.Areas[i].X / mCellSize;
                pos.Y = mBsp.Areas[i].Y / mCellSize;

                Point randomPoint = new Point();

                do
                {
                    randomPoint.X = mRand.Next(pos.X, pos.X + mBsp.Areas[i].Width / mCellSize - 1);
                    randomPoint.Y = mRand.Next(pos.Y, pos.Y + mBsp.Areas[i].Height / mCellSize - 1);
                } while (room.Contains(randomPoint));

                if (randomPoint.X % 2 == 0)
                    randomPoint.X++;
                if (randomPoint.Y % 2 == 0)
                    randomPoint.Y++;

                if (mRand.Next(1, 2) == 1)
                {
                    CreateHorizontalTunnel(previousRoomCenterX, randomPoint.X, previousRoomCenterY);
                    CreateVerticalTunnel(previousRoomCenterY, randomPoint.Y, randomPoint.X);
                }
                else
                {
                    CreateVerticalTunnel(previousRoomCenterY, randomPoint.Y, previousRoomCenterX);
                    CreateHorizontalTunnel(previousRoomCenterX, randomPoint.X, randomPoint.Y);
                }
            }*/
        }

        private void CreateRoomsTunnel()
        {
            // Iterate through each room that was generated
            // Don't do anything with the first room, so start at r = 1 instead of r = 0
            for (int r = 1; r < mRooms.Count; r++)
            {
                CreateRoomTunnel(mRooms[r - 1], mRooms[r]);
            }
        }

        private void CreateRoomTunnel(Room pRoomStart,Room pRoomEnd)
        {
            Point start = pRoomStart.Center;
            Point end = pRoomEnd.Center;

            int width = Math.Abs(end.X - start.X);
            int height = Math.Abs(end.Y - start.Y);

            if (width == 0)
                width = 1;
            if (height == 0)
                height = 1;

            int minX = Math.Min(start.X,end.X) - width / 2;
            int minY = Math.Min(start.Y, end.Y) - height / 2;

            int maxX = Math.Max(start.X, end.X) + width / 2;
            int maxY = Math.Max(start.Y, end.Y) + height / 2;

            minX = GrimMath.Clamp(minX, 1, NbCol - 1);
            minY = GrimMath.Clamp(minY, 1, NbLig - 1);

            maxX = GrimMath.Clamp(maxX, 1, NbCol - 1);
            maxY = GrimMath.Clamp(maxY, 1, NbLig - 1);

            int nbPoints = mRand.Next(0,5 + 1);

            Point previous = start;

            mTestPoints = new List<Point>();

            mTestPoints.Add(previous);

            for (int i = 0; i < nbPoints; i++)
            {
                Point p = new Point(-1, -1);

                p = GetRandomPathCell(new Point(minX, minY), new Point(maxX, maxY), mTestPoints);

                CreateTunnel(previous, p);

                previous = p;

                mTestPoints.Add(previous);
            }

            CreateTunnel(previous, end);

            mTestPoints.Add(end);
        }

        private Point GetRandomPathCell(Point pMin, Point pMax, List<Point> pPreviousCells)
        {
            Point cell = new Point(-1, -1);

            bool isValid = false;

            while (!isValid)
            {
                isValid = true;

                cell.X = mRand.Next(pMin.X, pMax.X + 1);
                cell.Y = mRand.Next(pMin.Y, pMax.Y + 1);

                if (cell.X % 2 == 0 || cell.Y % 2 == 0)
                    isValid = false;
                else if (Grid[cell.Y, cell.X] == eCellType.FLOOR_ROOM)
                    isValid = false;
                else if (pPreviousCells.Contains(cell))
                    isValid = false;
            }

            return cell;
        }

        private void CreateTunnel(Point pStart,Point pEnd)
        {
            // Give a 50/50 chance of which 'L' shaped connecting hallway to tunnel out
            if (mRand.Next(1, 2) == 1)
            {
                CreateHorizontalTunnel(pStart.X, pEnd.X, pStart.Y);
                CreateVerticalTunnel(pStart.Y, pEnd.Y, pEnd.X);
            }
            else
            {
                CreateVerticalTunnel(pStart.Y, pEnd.Y, pStart.X);
                CreateHorizontalTunnel(pStart.X, pEnd.X, pEnd.Y);
            }
        }

        // Carve a tunnel out of the map parallel to the x-axis
        private void CreateHorizontalTunnel(int xStart, int xEnd, int yPosition)
        {
            for (int x = Math.Min(xStart, xEnd); x <= Math.Max(xStart, xEnd); x++)
            {
                DigWall(x, yPosition);
            }
        }

        // Carve a tunnel out of the map parallel to the y-axis
        private void CreateVerticalTunnel(int yStart, int yEnd, int xPosition)
        {
            for (int y = Math.Min(yStart, yEnd); y <= Math.Max(yStart, yEnd); y++)
            {
                DigWall(xPosition, y);
            }
        }

        #endregion

        #region GrimEngine Methods

        public void Update(GameTime gameTime)
        {
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {

        }

        #endregion
    }
}
